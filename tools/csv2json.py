#!/usr/bin/env python3

import sys
import csv
import json


def convert(file_name):

    content = []
    with open(file_name, "r") as f:
        for line in csv.DictReader(f):
            content.append(line)
    print(json.dumps(content))

    return 0


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: {} FILE_NAME".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)
    sys.exit(convert(sys.argv[1]))
